# Cloud Image Browser

An image browser that is fully serverless and makes heavy use of AWS services to store, manage and analyze images.

# Progress

Have a look at the milestones and Issues. I try to keep at least the tickets of the current milestone up to date.

# Getting started

I have no proper guide here right now. But this will follow when the first tasks are done and there is something to get started with ;)

# Used technologies

- Tailwind CSS | Providing me with a lot of flexibility and the capability to make the app look unique and good

- Webpack | probably a bit bloated, but it is supported by a ton of people and it can be tweaked a lot to fit my needs