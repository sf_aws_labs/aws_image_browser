#!/bin/bash
set -e 

# Requires the change set name to start
if [ "$1" = "" ]; then
    echo "Correct use of the script: ./prepare.sh [change-set-name]"
    exit 1
fi
CHANGESETNAME=$1

aws cloudformation execute-change-set --change-set-name "$CHANGESETNAME" --stack-name image-browser