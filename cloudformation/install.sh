#!/bin/bash

aws cloudformation create-stack --template-body file//:basic-stack.yaml --stack-name image-browser
aws cloudformation wait stack-create-complete --stack-name image-browser