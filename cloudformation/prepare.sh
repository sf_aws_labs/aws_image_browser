#!/bin/bash
set -e 

# Requires the change set name to start
if [ "$1" = "" ]; then
    echo "Correct use of the script: ./prepare.sh [change-set-name]"
    exit 1
fi
CHANGESETNAME=$1

aws cloudformation create-change-set --template-body file://basic-stack.yaml --stack-name image-browser --change-set-name "$CHANGESETNAME"

echo 'Waiting for creation of the changeset to complete'
aws cloudformation wait change-set-create-complete --change-set-name "$CHANGESETNAME" --stack-name image-browser

echo 'Changeset Created.'
aws cloudformation describe-change-set --change-set-name "$CHANGESETNAME" --stack-name image-browser

echo "Run ./execute.sh $CHANGESETNAME to apply"